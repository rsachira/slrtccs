from google.appengine.ext import db
import logging

class TrackNumberTable(db.Model):
	trackno = db.StringProperty(required=True)
	name = db.StringProperty(required=True)

def addTrackNumberTable(track, n):
	d = TrackNumberTable(trackno = track, name = n)
	d.put()
	
	return True

def getTrainNumber():
	q = TrackNumberTable.all()
	t = q.run()
	
	l = []
	for qq in t:
		p = {}
		p['name'] = qq.name
		p['trackno'] = qq.trackno
		l.append(p)
		logging.debug(qq.name)
	
	return l