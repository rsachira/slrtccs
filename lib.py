import Graph
import TrainNote
import TrackData

def getGraphItems(fromTime, endTime, trackNum, train_id = 999):
	q = Graph.Graph.all()
	
	if(train_id == 999):	# if train_id = 999 no value given
		q.filter('track_no = ', trackNum)
		q.filter('time >', fromTime)
		q.filter('time <', endTime)
		q.order('time')
	else:
		q.filter('track_no = ', trackNum)
		q.filter('time >', fromTime)
		q.filter('time <', endTime)
		q.filter('train_id = ', train_id)
		q.order('time')
	
	return q.run()

def deleteGraphItems(fromTime, trackNum, graph_num, train_id):
	
	Graph.deleteGraphItems(fromTime, trackNum, graph_num, train_id)

def getTrainNotes(track):
	
	q = TrainNote.TrainNote.all()
	q.filter('track_no = ', track)
	
	return q.run()

def getAllTrainNotes():
	
	q = TrainNote.TrainNote.all()
	
	return q.run()

def getTrainNote(train_id, date):
	q = TrainNote.TrainNote.all()
	q.filter('train_id = ', train_id)
	q.filter('date =', date)
	
	return q.run()

def removeTrainNote(id):
	TrainNote.removeTrain(id)

def getTrainIds(fromTime, track):
	
	q = Graph.Graph.all()
	q.filter('track_no = ', track)
	q.filter('time >', fromTime)
	q.order('time')
	
	items = q.run()
	
	trainIDs = set([])
	for i in items:
		trainIDs.add(i.train_id)
	
	return trainIDs

def getTrackData(track):
	
	q = TrackData.TrackData.all()
	q.filter('track_no = ', track)
	q.order('graph_number')
	
	return q.run()