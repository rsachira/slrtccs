import datetime
from google.appengine.ext import db

class TrackData(db.Model):
	
	track_no = db.StringProperty(required=True)
	track_name = db.StringProperty(required=True)
	station_code = db.StringProperty()
	station_name = db.StringProperty(required=True)
	graph_number = db.IntegerProperty(required=True)
 	

def addTrackData(trackNum, trackName, sCode, sName, graphNum):
	
	t = TrackData(track_no = trackNum, track_name = trackName, station_code = sCode, station_name = sName, graph_number = graphNum)
	t.put()
