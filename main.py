#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import os

import datetime
import time
from google.appengine.ext import db

import jinja2
import webapp2
import Graph
import TrainNote
import lib
import TrackData
import logging

import user_api
import DefaultValues
import report

import copy
import collections

import TrackNumberTable
import Employee

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

class MainHandler(webapp2.RequestHandler):
    def get(self):
    	url = self.request.url
    	d = datetime.datetime.now()
    	
    	username = user_api.authenticate(self)
    	
    	template_values = {
    		
    		'url' : url,
    		'year' : d.year,
    		'month' : d.month,
    		'day' : d.day,
    		'usr' : username
    	}
    	
    	template = JINJA_ENVIRONMENT.get_template('home_page.html')
    	self.response.write( template.render(template_values) )

class SecondHandler(webapp2.RequestHandler):
    def get(self):
        url = self.request.url
        
        template_values = {
        	
        	'url' : url
        }
        
        template = JINJA_ENVIRONMENT.get_template('map_view.html')
        self.response.write( template.render(template_values) )

class NewTrainHandler(webapp2.RequestHandler):
	def get(self):
		
		drivers = Employee.getDrivers()
		guards = Employee.getGuards()
		coaches = Employee.getCoaches()
		
		train_no = TrackNumberTable.getTrainNumber()
		url = self.request.url
		template_values = {
			'title' : 'Add New Train',
			'url' : url,
			'train_no' : train_no,
			'drivers' : drivers,
			'guards' : guards,
			'coaches' : coaches
		}
		template = JINJA_ENVIRONMENT.get_template('train_note_add.html')
		self.response.write(template.render(template_values))
		
	def post(self):
		tID = self.request.get("tID")
		trackno = self.request.get("trackno")
		coaches = self.request.get("coaches")
		dName = self.request.get("dName")
		gName = self.request.get("gName")
		home = self.request.get("home")
		dest = self.request.get("dest")
		
		Y = int( self.request.get('y') )
		M = int( self.request.get('m') )
		D = int( self.request.get('d') )
		
		date = datetime.datetime(Y, M, D)
		
		TrainNote.addTrain(tID, dName, gName, date, home, trackno, coaches, dest)
		self.redirect(self.request.url)

class RemoveTrainHandler(webapp2.RequestHandler):
	def get(self):
		id = self.request.get("id")
		lib.removeTrainNote(id)
		
		self.redirect('/update_train')

class TrainNoteUpdate(webapp2.RequestHandler):
	def get(self):
		tID = self.request.get("tID")
		train_no = TrackNumberTable.getTrainNumber()
		drivers = Employee.getDrivers()
		guards = Employee.getGuards()
		coaches = Employee.getCoaches()
		
		try:
			Y = int( self.request.get('y') )
			M = int( self.request.get('m') )
			D = int( self.request.get('d') )
			
			date = datetime.datetime(Y, M, D)
		except:
			Y = 0
			M = 0
			D = 0
			
			date = datetime.datetime(2010, 10, 10)
		
		q = lib.getTrainNote(tID, date)
		
		data = {}
		id = ''
		for p in q:
			data = p
			id = p.key()
		
		template_values = {
			'data' : data,
			'id' : id,
			'url' : self.request.url,
			'y' : Y,
			'm' : M,
			'd' : D,
			'train_no' : train_no,
			'drivers' : drivers,
			'guards' : guards,
			'coaches' : coaches
		}
		template = JINJA_ENVIRONMENT.get_template('train_note_update.html')
		self.response.write(template.render(template_values))
	
	def post(self):
		tID = self.request.get("tID")
		trackno = self.request.get("trackno")
		coaches = self.request.get("coaches")
		dName = self.request.get("dName")
		gName = self.request.get("gName")
		home = self.request.get("home")
		dest = self.request.get("dest")
		
		Y = int( self.request.get('y') )
		M = int( self.request.get('m') )
		D = int( self.request.get('d') )
		
		id = self.request.get("id")
		
		date = datetime.datetime(Y, M, D)
		
		lib.removeTrainNote(id)
		TrainNote.addTrain(tID, dName, gName, date, home, trackno, coaches, dest)
		self.redirect(self.request.url)

class RemoveItem(webapp2.RequestHandler):
	def get(self):
		tID = self.request.get("tID")
		g_num = self.request.get("g_num")
		trackno = self.request.get("trackno")
		
		Y = int( self.request.get('y') )
		M = int( self.request.get('m') )
		D = int( self.request.get('d') )
		
		todayDate = datetime.datetime(Y, M, D)
		lib.deleteGraphItems(todayDate, trackno, g_num, tID)
		self.redirect('/datatable?trackno=' + trackno + '&y=' + str(Y) + '&m=' + str(M) + '&d=' + str(D) + '&tID=' + tID)
		

class DataTable(webapp2.RequestHandler):
	def get(self):
		tID = self.request.get("tID")
		trackno = self.request.get("trackno")
		
		Y = int( self.request.get('y') )
		M = int( self.request.get('m') )
		D = int( self.request.get('d') )
		
		todayDate = datetime.datetime(Y, M, D)
		endDate = todayDate + datetime.timedelta(days = 1)
		data = lib.getGraphItems(todayDate, endDate, trackno, tID)
		
		template_values = {
			'y' : Y,
			'm' : M,
			'd' : D,
			'train_id' : tID,
			'items' : data,
			'trackno' : trackno
		}
		template = JINJA_ENVIRONMENT.get_template('data_table.html')
		self.response.write(template.render(template_values))

class NewGraphItem(webapp2.RequestHandler):
	def get(self):
		
		tNotes = lib.getAllTrainNotes()
		url = self.request.url
		template_values = {
			'trainNotes' : tNotes,
			'url' : url
		}
		template = JINJA_ENVIRONMENT.get_template('new_item.html')
		self.response.write(template.render(template_values))
		
		
	def post(self):
		
		tID = self.request.get("train_id")
		track = self.request.get("track")
		sCode = self.request.get("sCode")
		aTime = self.request.get("aTime")
		dTime = self.request.get("dTime")
		
		a_comment = self.request.get("a_comment")
		d_comment = self.request.get("d_comment")
		
		url = self.request.get("url")
		
		arrival = datetime.datetime.strptime(aTime, '%m/%d/%Y %H:%M')
		departure = datetime.datetime.strptime(dTime, '%m/%d/%Y %H:%M')
		
		logging.debug(sCode)
		logging.debug(track)
		
		t = TrackData.TrackData.all()
		t.filter('track_no = ', track)
		t.filter('station_name = ', sCode)
		data = t.run()
		
		r = False
		
		for g in data:
			r = Graph.addGraphItem(tID, sCode, track, g.graph_number, arrival, departure, a_comment, d_comment)
			break
		
		if(r is True):
			self.response.write("Success")
			self.redirect( str(url) )

class GraphView(webapp2.RequestHandler):
	def get(self):
		track = self.request.get('trackno')
		Y = int( self.request.get('y') )
		M = int( self.request.get('m') )
		D = int( self.request.get('d') )
		
		comp_page = int( self.request.get('comp_page') )
		logging.debug(comp_page)
		if comp_page is None:
			comp_page = 0
		
		todayDate = datetime.datetime(Y, M, D)
		tomorrow = todayDate + datetime.timedelta(days = 1)
		
		q = lib.getGraphItems(todayDate, tomorrow, track)
		trainIDs = lib.getTrainIds(todayDate, track) #lib.getTrainNotes(track)
		trackData = lib.getTrackData(track)
		
		url = self.request.url
		
		data = {}
		
		zeroTime = time.mktime( todayDate.timetuple() )
		
		defVal = {}
		
		for trainID in trainIDs:
			defVal[trainID] = {}
			defVal[trainID]['station_code'] = ''
			defVal[trainID]['graph_num'] = ''
		
		for d in q:
			deltaT = str( time.mktime( d.time.timetuple() ) - zeroTime )
			deltaT = d.time
			data[deltaT] = data.get(deltaT, 0)
			if( data[deltaT] == 0 ):
				data[deltaT] = copy.deepcopy(defVal)
			
			data[deltaT][d.train_id]['station_code'] = d.station_code
			data[deltaT][d.train_id]['graph_num'] = d.graph_num
		
		data = collections.OrderedDict( sorted( data.items() ) )
		
		tNotes = lib.getAllTrainNotes()
		
		template_values = {
			'trains' : trainIDs,
			'items' : data,
			'trackData' : trackData,
			'trackData_' : lib.getTrackData(track),
			'trainNotes' : tNotes,
			'url' : url,
			'track' : track,
			'y' : Y,
			'm' : M,
			'd' : D,
			'comp_page' : comp_page
		}
		template = JINJA_ENVIRONMENT.get_template('graph1.html')
		self.response.write(template.render(template_values))
		#self.response.write(data);

class CompareView(webapp2.RequestHandler):
	def post(self):
		date_one = self.request.get("date_one")
		date_two = self.request.get("date_two")
		track = self.request.get("track")
		
		d1 = datetime.datetime.strptime(date_one, '%m/%d/%Y')
		d2 = datetime.datetime.strptime(date_two, '%m/%d/%Y')
		
		url = self.request.url
		template_values = {
			'url' : url,
			'done' : True,
			
			'y1' : d1.year,
			'm1' : d1.month,
			'd1' : d1.day,
			
			'y2' : d2.year,
			'm2' : d2.month,
			'd2' : d2.day,
			
			'track' : track
		}
		template = JINJA_ENVIRONMENT.get_template('graph_comp.html')
		self.response.write(template.render(template_values))
		
	def get(self):
		track = self.request.get("track")
		
		url = self.request.url
		template_values = {
			'done' : False,
			'url' : url,
			'track' : track
		}
		
		template = JINJA_ENVIRONMENT.get_template('graph_comp.html')
		self.response.write(template.render(template_values))

class NewTrackData(webapp2.RequestHandler):
	def get(self):
		url = self.request.url
		template_values = {
			'url' : url
		}
		template = JINJA_ENVIRONMENT.get_template('new_graph_item_template.html')
		self.response.write(template.render(template_values))
		
	def post(self):
		
		trackNum = self.request.get("trackNum")
		trackName = self.request.get("trackName")
		sCode = self.request.get("sCode")
		sName = self.request.get("sName")
		graphNum = self.request.get("graphNum")
		
		TrackData.addTrackData(trackNum, trackName, sCode, sName, int(graphNum) )
		
		self.redirect(self.request.url)

class UserManager(webapp2.RequestHandler):
	def get(self):
		url = self.request.url
		
		users = {}
		i = 0
		for p in user_api.getAllUsers():
			users[i] = {}
			users[i]['username'] = p.username
			users[i]['id'] = p.key()
			i += 1
		
		template_values = {
			'url' : url,
			'users' : users
		}
		template = JINJA_ENVIRONMENT.get_template('user_manager.html')
		self.response.write(template.render(template_values))
	
	def post(self):
		email = self.request.get("username")
		user_api.addUser(email)
		
		self.redirect(self.request.url)

class RemoveUser(webapp2.RequestHandler):
	def get(self):
		user_id = self.request.get("key")
		user_api.removeUser(user_id)
		self.redirect('/admin/usermanager')

class AssistancePage(webapp2.RequestHandler):
	def get(self):
		username = user_api.authenticate(self)
		
		template_values = {
			'users' : username
		}
		
		template = JINJA_ENVIRONMENT.get_template('assistance_page.html')
		self.response.write(template.render(template_values))

class NewDefaultItem(webapp2.RequestHandler):
	def get(self):
		
		tNotes = lib.getAllTrainNotes()
		url = self.request.url
		template_values = {
			'trainNotes' : tNotes,
			'url' : url
		}
		template = JINJA_ENVIRONMENT.get_template('Default_data.html')
		self.response.write(template.render(template_values))
		
	def post(self):
		
		tID = self.request.get("train_id")
		track = self.request.get("track")
		sCode = self.request.get("sCode")
		aTime = self.request.get("aTime")
		dTime = self.request.get("dTime")
		
		arrival = datetime.datetime.strptime(aTime, '%H:%M')
		departure = datetime.datetime.strptime(dTime, '%H:%M')
		
		r = DefaultValues.addDefValue(tID, sCode, track, arrival.time() )
		r = DefaultValues.addDefValue(tID, sCode, track, departure.time() )
		
		if(r is True):
			self.response.write("Success")
			self.redirect(self.request.uri)

class AddEmployees(webapp2.RequestHandler):
	def get(self):
		url = self.request.url
		
		template_values = {
			'url' : url
		}
		template = JINJA_ENVIRONMENT.get_template('Master_data.html')
		self.response.write(template.render(template_values))
	
	def post(self):
		dName = self.request.get("dName")
		gName = self.request.get("gName")
		
		if gName != '':
			Employee.addGuard(gName)
		if dName != '':
			Employee.addDriver(dName)
		
		self.response.write("Success")
		self.redirect(self.request.uri)

class ReportGeneration(webapp2.RequestHandler):
	def get(self):
		url = self.request.url
		
		template_values = {
			'url' : url,
			'table' : {}
		}
		template = JINJA_ENVIRONMENT.get_template('report_new.html')
		self.response.write(template.render(template_values))
	
	def post(self):
		trainId = self.request.get("tID")
		track = self.request.get("track")
		date = self.request.get("date")
		
		logging.debug(track)
		logging.debug(trainId)
		logging.debug(date)
		
		dateLower = datetime.datetime.strptime(date, '%m/%d/%Y')
		dateUpper = dateLower + datetime.timedelta(days=1)
		
		actual = report.getActualData(trainId, track, dateUpper, dateLower)
		default = report.getDefaultData(trainId, track)
		
		table = {}
		i = 0
		for p in default:
			table[i] = {}
			table[i]['station'] = p.station_code
			table[i]['default'] = p.time
			i += 1
		
		j = 0;
		for p in actual:
			table[j]['actual'] = p.time.time()
			table[j]['gap'] = report.getTimeGap(table[j]['actual'], table[j]['default'])
			table[j]['comment'] = p.comments
			j += 1
		
		big_table = {}
		l = 0;
		for k in xrange(1, j, 2):
			big_table[l] = {}
			big_table[l]['arrival'] = table[k - 1]
			big_table[l]['departure'] = table[k]
			l += 1
		
		Y = dateLower.year
		M = dateLower.month
		D = dateLower.day
		
		template_values = {
			'table' : big_table,
			'Y' : Y,
			'M' : M,
			'D' : D,
			'train_id' : trainId
		}
		template = JINJA_ENVIRONMENT.get_template('report_new.html')
		self.response.write(template.render(template_values))

app = webapp2.WSGIApplication([
    ('/', MainHandler),
    ('/mapview', SecondHandler),
    ('/newtrain', NewTrainHandler),
    ('/newitem', NewGraphItem),
    ('/trackdata', NewTrackData),
    ('/graph', GraphView),
    ('/removeItem', RemoveItem),
    ('/datatable', DataTable),
    ('/assistance', AssistancePage),
    ('/report', ReportGeneration),
    ('/update_train', TrainNoteUpdate),
    ('/remove_train', RemoveTrainHandler),
	('/compare', CompareView)
], debug=True)

admin_app = webapp2.WSGIApplication([
    ('/admin/usermanager', UserManager),
    ('/admin/removeuser', RemoveUser),
    ('/admin/defaults', NewDefaultItem),
    ('/admin/add_employees', AddEmployees)
], debug=True)