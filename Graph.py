import TrainNote
import datetime
from google.appengine.ext import db

import logging

class Graph(db.Model):
	
	train_id = db.StringProperty(required=True)
	station_code = db.StringProperty(required=True)
	track_no = db.StringProperty(required=True, indexed=True)
	graph_num = db.IntegerProperty(required=True)
	time = db.DateTimeProperty(required=True, indexed=True)
	
	comments = db.StringProperty(required=False)

def addGraphItem(tID, sCode, trackNum, graphNum, arrival, departure, comm_arrival = '', com_departure = ''):
	
	'''train = TrainNote.TrainNote.get_by_key_name(tID)
	if(train is None):
		return False
	'''
	g1 = Graph(train_id = tID, station_code = sCode, track_no = trackNum, graph_num = graphNum, time = arrival, comments = comm_arrival)
	g2 = Graph(train_id = tID, station_code = sCode, track_no = trackNum, graph_num = graphNum, time = departure, comments = com_departure)
	g1.put()
	g2.put()
	
	return True

def deleteGraphItems(fromTime, trackNum, graph_num, train_id):
	
	logging.debug('deleteGraphItems')
	logging.debug(trackNum)
	logging.debug(fromTime)
	logging.debug(graph_num)
	logging.debug(train_id)
	
	q = Graph.all()
	q.filter('track_no = ', str(trackNum) )
	q.filter('time >', fromTime)
	q.filter('graph_num = ', int(graph_num) )
	q.filter('train_id = ', str(train_id) )
	q.order('time')
	
	db.delete( q.run() )