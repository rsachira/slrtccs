import TrainNote
from google.appengine.ext import db

class DefaultValue(db.Model):
	train_id = db.StringProperty(required=True)
	station_code = db.StringProperty(required=True)
	track_no = db.StringProperty(required=True, indexed=True)
	
	time = db.TimeProperty(required=True, indexed=True)

def addDefValue(trainId, stationCode, track, t):
	
	d = DefaultValue(train_id = trainId, station_code = stationCode, track_no = track, time = t)
	d.put()
	
	return True

def deleteDefValue(train_id, station_code, track_no):
	
	q = DefaultValue.all()
	q.filter('track_no = ', str(track_no) )
	q.filter('train_id = ', str(train_id) )
	q.order('station_code = ', str(station_code) )
	
	db.delete( q.run() )