import datetime
from google.appengine.ext import db

class TrainNote(db.Model):
	
	date = db.DateTimeProperty(required=True)
	
	track_no = db.StringProperty(required=True)
	train_id = db.StringProperty(required=True)
	driver_name = db.StringProperty(required=True)
	guard_name = db.StringProperty(required=True)
	home_station = db.StringProperty(required=True)
	destination = db.StringProperty(required=True)
	
	coach_type = db.StringProperty(required=True)

def addTrain(tID, dName, gName, date_added, station, track, coaches, dest):
	
	g = TrainNote(train_id = tID, driver_name = dName, guard_name = gName, date = date_added, home_station = station, track_no = track, coach_type = coaches, destination = dest)
	g.put()

def removeTrain(id):
	q = TrainNote.get(id)
	db.delete(q)

def populateTable():
	
	addTrain('too2', 'tn002', 'abc', 'drv001', 'en001', 'FRT', '2', 'emp001')
	addTrain('t003', 'tn003', 'abd', 'drv002', 'en002', 'FRT', '2', 'emp001')
	addTrain('t004', 'tn004', 'abe', 'drv003', 'en003', 'FRT', '2', 'emp001')
	'''addTrain('t005', 'tn005', 'abf', 'drv004', 'en004', 'FRT', 'emp001')
	addTrain('t006', 'tn006', 'abg', 'drv005', 'en005', 'FRT', 'emp001')
	addTrain('t007', 'tn007', 'abh', 'drv006', 'en006', 'FRT', 'emp001')
	addTrain('t008', 'tn008', 'abi', 'drv007', 'en007', 'FRT', 'emp001')
	addTrain('t009', 'tn009', 'abj', 'drv008', 'en008', 'FRT', 'emp001')
	addTrain('t010', 'tn010', 'abk', 'drv009', 'en009', 'FRT', 'emp001')
	addTrain('t011', 'tn011', 'abl', 'drv010', 'en010', 'FRT', 'emp001')
	addTrain('t012', 'tn012', 'abm', 'drv011', 'en011', 'FRT', 'emp002')
	addTrain('t013', 'tn013', 'abn', 'drv012', 'en012', 'FRT', 'emp002')
	addTrain('t014', 'tn014', 'abo', 'drv013', 'en013', 'FRT', 'emp002')
	addTrain('t015', 'tn015', 'abp', 'drv014', 'en014', 'FRT', 'emp002')
	addTrain('t016', 'tn016', 'abq', 'drv015', 'en015', 'FRT', 'emp002')
	addTrain('t017', 'tn017', 'abr', 'drv016', 'en016', 'FRT', 'emp002')
	addTrain('t018', 'tn018', 'abs', 'drv017', 'en017', 'FRT', 'emp002')
	addTrain('t019', 'tn019', 'abt', 'drv018', 'en018', 'FRT', 'emp002')
	addTrain('t020', 'tn020', 'abu', 'drv019', 'en019', 'FRT', 'emp002')
	addTrain('t021', 'tn021', 'abv', 'drv020', 'en020', 'FRT', 'emp002')
	addTrain('t022', 'tn022', 'abw', 'drv021', 'en021', 'FRT', 'emp002')
	addTrain('t023', 'tn023', 'abx', 'drv022', 'en022', 'FRT', 'emp002')
	addTrain('t024', 'tn024', 'aby', 'drv023', 'en023', 'FRT', 'emp002')
	addTrain('t025', 'tn025', 'abz', 'drv024', 'en024', 'FRT', 'emp002')
	addTrain('t026', 'tn026', 'aca', 'drv025', 'en025', 'FRT', 'emp003')
	addTrain('t027', 'tn027', 'acb', 'drv026', 'en026', 'FRT', 'emp003')
	addTrain('t028', 'tn028', 'acc', 'drv027', 'en027', 'FRT', 'emp003')
	addTrain('t029', 'tn029', 'acd', 'drv028', 'en028', 'FRT', 'emp003')
	addTrain('t030', 'tn030', 'ace', 'drv029', 'en029', 'FRT', 'emp003')
	addTrain('t031', 'tn032', 'acf', 'drv030', 'en030', 'FRT', 'emp003')
	addTrain('t032', 'tn032', 'acg', 'drv031', 'en031', 'FRT', 'emp003')
	addTrain('t033', 'tn033', 'ach', 'drv032', 'en032', 'FRT', 'emp003')
	addTrain('t034', 'tn034', 'aci', 'drv033', 'en033', 'DMG', 'emp003')
	addTrain('t034', 'tn034', 'aci', 'drv033', 'en033', 'DMG', 'emp003')
	addTrain('t035', 'tn035', 'acj', 'drv034', 'en034', 'DMG', 'emp003')
	addTrain('t036', 'tn036', 'ack', 'drv035', 'en035', 'DMG', 'emp003')
	addTrain('t037', 'tn037', 'acl', 'drv036', 'en036', 'DMG', 'emp003')
	addTrain('t038', 'tn038', 'acm', 'drv037', 'en037', 'DMG', 'emp004')
	addTrain('t039', 'tn039', 'acn', 'drv038', 'en038', 'DMG', 'emp004')
	addTrain('t040', 'tn040', 'aco', 'drv039', 'en039', 'DMG', 'emp004')
	addTrain('t041', 'tn041', 'acp', 'drv040', 'en040', 'DMG', 'emp004')
	addTrain('t042', 'tn042', 'acq', 'drv041', 'en041', 'DMG', 'emp004')
	addTrain('t043', 'tn043', 'acr', 'drv042', 'en042', 'DMG', 'emp004')
	addTrain('t044', 'tn044', 'acs', 'drv043', 'en043', 'DMG', 'emp004')
	addTrain('t045', 'tn045', 'act', 'drv044', 'en044', 'DMG', 'emp004')
	addTrain('t046', 'tn046', 'acu', 'drv045', 'en045', 'DMG', 'emp004')
	addTrain('t047', 'tn047', 'acv', 'drv046', 'en046', 'DMG', 'emp004')
	addTrain('t048', 'tn048', 'acw', 'drv047', 'en047', 'DMG', 'emp004')
	addTrain('t049', 'tn049', 'acx', 'drv048', 'en048', 'DMG', 'emp004')
	addTrain('t050', 'tn050', 'acy', 'drv049', 'en049', 'DMG', 'emp004')'''