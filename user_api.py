from google.appengine.api import users
from google.appengine.ext import db

class UserDetails(db.Model):
	
	username = db.EmailProperty(required=True)

def addUser(email):
	usr = UserDetails(username = db.Email(email) )
	usr.put()

def removeUser(key):
	usr = UserDetails.get(key)
	db.delete(usr)

def getAllUsers():
	users = UserDetails.all()
	return users.run()

def isRegisteredUser(email):
	
	q = UserDetails.all()
	q.filter('username = ', db.Email(email) )
	usr = q.get()
	
	if usr is None:
		return False
	
	return True

def authenticate(webapp):
	user = users.get_current_user()
	
	if user:
		if isRegisteredUser( user.email() ):
			return user.nickname()
		else:
			webapp.redirect( users.create_logout_url(webapp.request.uri) )
	else:
		webapp.redirect( users.create_login_url(webapp.request.uri) )

def isAdmin(webapp, redirect = False):
	
	if users.is_current_user_admin():
		return True
	
	if redirect is True:
		webapp.redirect( users.create_login_url(webapp.request.uri) )
	
	return False