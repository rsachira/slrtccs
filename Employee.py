import datetime
from google.appengine.ext import db

class Driver(db.Model):
	name = db.StringProperty(required=True)
 	

def addDriver(n):
	
	t = Driver(name = n)
	t.put()

def getDrivers():
	q = Driver.all()
	t = q.run()
	
	l = []
	for qq in t:
		p = {}
		p['name'] = qq.name
		l.append(p)
	
	return l

class Guard(db.Model):
	name = db.StringProperty(required=True)
 	
def addGuard(n):
	
	t = Guard(name = n)
	t.put()

def getGuards():
	q = Guard.all()
	t = q.run()
	
	l = []
	for qq in t:
		p = {}
		p['name'] = qq.name
		l.append(p)
	
	return l

# Coaches types
class Coach(db.Model):
	type = db.StringProperty(required=True)
 	

def addCoach(n):
	
	t = Coach(type = n)
	t.put()

def getCoaches():
	q = Coach.all()
	t = q.run()
	
	l = []
	for qq in t:
		p = {}
		p['type'] = qq.type
		l.append(p)
	
	return l
