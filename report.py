import datetime
import Graph
import DefaultValues

from google.appengine.ext import db

def getActualData(trainId, track, dUp, dLow):
	q = Graph.Graph.all()
	
	q.filter('track_no = ', str(track) )
	q.filter('train_id = ', str(trainId) )
	q.filter('time >', dLow)
	q.filter('time <', dUp)
	q.order('time')
	
	return q.run()

def getDefaultData(trainId, track):
	q = DefaultValues.DefaultValue.all()
	
	q.filter('train_id = ', trainId)
	q.filter('track_no = ', track)
	q.order('time')
	
	return q.run()

def getTimeGap(actual, default):
	dt = datetime.datetime.combine(datetime.date.today(), actual ) - datetime.datetime.combine(datetime.date.today(), default )
	return dt